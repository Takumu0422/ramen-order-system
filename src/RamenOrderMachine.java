import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;

/* classで商品の要素をつくる */
class Item {
    String name;
    int price;

    public Item(String name, int price) {
        this.name = name;
        this.price = price;
    }
}

public class RamenOrderMachine extends JPanel {
    private JPanel root;
    private JLabel topLabel;
    private JButton ramen￥800Button;
    private JButton tukemen￥830Button;
    private JButton mazesoba￥780Button;
    private JButton toripaitan￥900Button;
    private JButton mazesobaCurryFlavor￥850Button;
    private JButton charSiuBento￥500Button;
    private JButton checkOutButton;
    private JTextField totalPrice;
    private JTextArea list1;
    private final Item[] items;
    private Item[] orders;
    int i = 0;
    int sum;
    int numberOfPeople=1;

    void order(Item item) {
        if (i >= numberOfPeople) {
            JOptionPane.showMessageDialog(null, "You can order up to " + numberOfPeople + " items.");
            return;
        }

        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + item.name + " ?",
                "order confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering "+ item.name +"."+" It will be served as soon as possible.");
            orders[i] = item;
            i++;
        }

        list1.setText("");
        sum = 0;

        for (int j = 0; j < i; j++) {
            String ordtext = list1.getText();
            list1.setText(ordtext + "・" + orders[j].name + " ¥" + orders[j].price + "\n");
            sum += orders[j].price;
        }
        totalPrice.setText("Total:¥" + sum);
    }

    public RamenOrderMachine() {
        items = new Item[]{
                new Item( "ramen", 800),
                new Item( "tukemen",830),
                new Item( "mazesoba",780),
                new Item( "toripaitan", 900),
                new Item( "mazesoba(Curry Flavor)",850),
                new Item( "char Siu Bento", 500)
        };

        orders = new Item[numberOfPeople];
        list1.setText("");
        totalPrice.setText("Total:¥" + sum);

        root.addComponentListener(new ComponentAdapter() {
        });

        ramen￥800Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(items[0]);
            }
        });
        ramen￥800Button.setIcon(new ImageIcon(this.getClass().getResource("ramen.png")));

        tukemen￥830Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(items[1]);
            }
        });
        tukemen￥830Button.setIcon(new ImageIcon(this.getClass().getResource("tukemen.png")));

        mazesoba￥780Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(items[2]);
            }
        });
        mazesoba￥780Button.setIcon(new ImageIcon(this.getClass().getResource("mazesoba.png")));

        toripaitan￥900Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(items[3]);
            }
        });
        toripaitan￥900Button.setIcon(new ImageIcon(this.getClass().getResource("toripaitan.png")));

        mazesobaCurryFlavor￥850Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(items[4]);
            }
        });
        mazesobaCurryFlavor￥850Button.setIcon(new ImageIcon(this.getClass().getResource("mazesoba(curry).png")));

        charSiuBento￥500Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(items[5]);
            }
        });
        charSiuBento￥500Button.setIcon(new ImageIcon(this.getClass().getResource("charsiubento.png")));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (i < numberOfPeople) {
                    JOptionPane.showMessageDialog(null, "Please order for all people.");
                    return;
                }

                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout confirmation",
                        JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is ￥"+sum);
                    list1.setText("");
                    sum = 0;
                    i = 0;
                    totalPrice.setText("Total:¥" + sum);
                }
            }
        });
    }

    private void initNumberOfPeople() {
        while (true) {
            String input = JOptionPane.showInputDialog("Enter the number of people:");
            try {
                numberOfPeople = Integer.parseInt(input);
                if (numberOfPeople < 1 || numberOfPeople > 4) {
                    throw new NumberFormatException();
                }
                orders = new Item[numberOfPeople]; // 注文リストを人数分に更新
                break; // 有効な入力が得られたらループを抜ける
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Invalid number. Please enter a valid number of people.");
            }
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("GUI Order System");
        RamenOrderMachine guiOrderSystem = new RamenOrderMachine();
        guiOrderSystem.initNumberOfPeople();
        frame.setContentPane(guiOrderSystem.root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
